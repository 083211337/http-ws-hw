const username = sessionStorage.getItem("username");

if (!username) {
  window.location.replace("/login");
}

const createRoomButton = document.querySelector('.create-room-button');

createRoomButton.addEventListener('click', function(){
  let newRoomName = prompt('you need create room name');

  if(!newRoomName)
  {
    alert('room you need write room name!');
    return ;  
  }
  let rooms = document.querySelector('rooms')
  let createBorderForNewRoom = document.createElement('div');
  let createRoomHeader = document.createElement('h3');
  let createRoomName = document.createElement('h1');
  const createJoinButton = document.createElement('button');

  createRoomHeader.innerText('user connected');
  createRoomName.innerText(newRoomName);
  createJoinButton.innerText('Join');
  createBorderForNewRoom.appendChild(createRoomHeader);
  createBorderForNewRoom.appendChild(createRoomName);
  createBorderForNewRoom.appendChild(createJoinButton);
  rooms.appendChild(createBorderForNewRoom);
})

const socket = io("", { query: { username } });